package com.example.definelabsassignment.model;

public class Icon {
    private String prefix,suffix;
    public Icon(){
        prefix="";
        suffix="";
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }
}
