package com.example.definelabsassignment.model;

public class Contact {
    private String phone, formattedPhone;

    public Contact() {
        phone = "";
        formattedPhone = "";
    }

    public String getFormattedPhone() {
        return formattedPhone;
    }

    public void setFormattedPhone(String formattedPhone) {
        this.formattedPhone = formattedPhone;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
