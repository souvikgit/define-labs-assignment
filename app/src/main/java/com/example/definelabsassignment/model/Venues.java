package com.example.definelabsassignment.model;

import java.util.LinkedList;
import java.util.List;

public class Venues {
    private String id,name,referralId;
    private boolean hasPerk,verified;
    private Contact contact;
    private Location location;
    private List<Categories> categories;
    private Stats stats;
    private VenuePage venuePage;
    private BeenHere beenHere;
    private List<Object> venueChains;
    private HereNow hereNow;

    public Venues() {
        id = "";
        name = "";
        verified = false;
        referralId = "";
        hasPerk = false;
        contact = new Contact();
        location = new Location();
        categories = new LinkedList<>();
        stats = new Stats();
        venuePage = new VenuePage();
        beenHere = new BeenHere();
        venueChains = new LinkedList<>();
        hereNow = new HereNow();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReferralId() {
        return referralId;
    }

    public void setReferralId(String referralId) {
        this.referralId = referralId;
    }

    public boolean isHasPerk() {
        return hasPerk;
    }

    public void setHasPerk(boolean hasPerk) {
        this.hasPerk = hasPerk;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public List<Categories> getCategories() {
        return categories;
    }

    public void setCategories(List<Categories> categories) {
        this.categories = categories;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public Stats getStats() {
        return stats;
    }

    public void setStats(Stats stats) {
        this.stats = stats;
    }

    public VenuePage getVenuePage() {
        return venuePage;
    }

    public void setVenuePage(VenuePage venuePage) {
        this.venuePage = venuePage;
    }

    public BeenHere getBeenHere() {
        return beenHere;
    }

    public void setBeenHere(BeenHere beenHere) {
        this.beenHere = beenHere;
    }

    public List<Object> getVenueChains() {
        return venueChains;
    }

    public void setVenueChains(List<Object> venueChains) {
        this.venueChains = venueChains;
    }

    public HereNow getHereNow() {
        return hereNow;
    }

    public void setHereNow(HereNow hereNow) {
        this.hereNow = hereNow;
    }
}



class BeenHere{
    int lastCheckinExpiredAt;

    public BeenHere() {
        lastCheckinExpiredAt = 0;
    }

    public int getLastCheckinExpiredAt() {
        return lastCheckinExpiredAt;
    }

    public void setLastCheckinExpiredAt(int lastCheckinExpiredAt) {
        this.lastCheckinExpiredAt = lastCheckinExpiredAt;
    }

}


class VenuePage{
    int id;

    public VenuePage(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}


class HereNow{
    int count;
    String summary;
    List<String> groups;

    HereNow(){
        count =0;
        summary="";
        groups = new LinkedList<>();
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public List<String> getGroups() {
        return groups;
    }

    public void setGroups(List<String> groups) {
        this.groups = groups;
    }
}