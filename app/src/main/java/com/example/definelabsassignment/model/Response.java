package com.example.definelabsassignment.model;

import java.util.LinkedList;
import java.util.List;

public class Response {
    private List<Venues> venues;
    private boolean confident;

    public Response() {
        venues = new LinkedList<>();
        confident = false;
    }

    public List<Venues> getVenues() {
        return venues;
    }

    public void setVenues(List<Venues> venues) {
        this.venues = venues;
    }

    public boolean isConfident() {
        return confident;
    }

    public void setConfident(boolean confident) {
        this.confident = confident;
    }
}
