package com.example.definelabsassignment.model;

public class Stats {
    private int tipCount,usersCount,checkinsCount;

    public Stats() {
        tipCount = 0;
        usersCount = 0;
        checkinsCount = 0;
    }

    public int getTipCount() {
        return tipCount;
    }

    public void setTipCount(int tipCount) {
        this.tipCount = tipCount;
    }

    public int getUsersCount() {
        return usersCount;
    }

    public void setUsersCount(int usersCount) {
        this.usersCount = usersCount;
    }

    public int getCheckinsCount() {
        return checkinsCount;
    }

    public void setCheckinsCount(int checkinsCount) {
        this.checkinsCount = checkinsCount;
    }
}
