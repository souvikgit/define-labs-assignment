package com.example.definelabsassignment.model;

public class JsonResponse {
    private Response response;

    public JsonResponse(){
        response = new Response();
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }
}
