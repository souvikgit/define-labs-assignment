package com.example.definelabsassignment.viewmodel;

import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.definelabsassignment.MainActivity;
import com.example.definelabsassignment.model.LocalData;

import java.util.List;

public class SavedMatchesViewModel extends ViewModel {
    // TODO: Implement the ViewModel
    private MutableLiveData<List<LocalData>> dataList;

    public LiveData<List<LocalData>> getDataList() {
        return dataList;
    }

    public void getAllSavedData() {
        dataList =  new MutableLiveData<>();
        dataList.setValue(MainActivity.localDatabase.myDao().getMatchData());
    }
}