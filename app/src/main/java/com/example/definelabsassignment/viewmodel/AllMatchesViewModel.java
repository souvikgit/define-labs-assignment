package com.example.definelabsassignment.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.definelabsassignment.model.JsonResponse;
import com.example.definelabsassignment.repository.AllMatchRepository;

import org.json.JSONArray;

public class AllMatchesViewModel extends ViewModel {
    private MutableLiveData<JsonResponse> jsonArrayMutableLiveData;

    public LiveData<JsonResponse> getMatchDetails(){
        return jsonArrayMutableLiveData;
    }

    public void initiateSearch(){
        jsonArrayMutableLiveData = new MutableLiveData<>();
        jsonArrayMutableLiveData = AllMatchRepository.getInstance().getAllMatchData();
    }
}