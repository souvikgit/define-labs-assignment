package com.example.definelabsassignment.repository;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.definelabsassignment.model.LocalData;

import java.util.List;

@Dao
public interface LocalDao {

    @Insert
    public void insert(LocalData match);

    @Query("Select * from `match`")
    public List<LocalData> getMatchData();

    @Query("Select count(1) from `match` where id =:id")
    public int isIdPresent(String id);

    @Query("delete from `match` where id =:id")
    public int deleteItem(String id);
}
