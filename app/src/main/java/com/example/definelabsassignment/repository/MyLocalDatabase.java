package com.example.definelabsassignment.repository;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.definelabsassignment.model.LocalData;

@Database(entities = {LocalData.class}, version = 1)
public abstract class MyLocalDatabase extends RoomDatabase {
    public abstract LocalDao myDao();
}
