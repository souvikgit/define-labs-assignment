package com.example.definelabsassignment.repository;

import com.example.definelabsassignment.model.JsonResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NetworkCall {
    String BASE_URL = "https://api.foursquare.com/v2/venues/";

    @GET("search")
    Call<JsonResponse> getAllMatches(@Query("ll") String ll, @Query("oauth_token") String authToken, @Query("v") String v);
}
