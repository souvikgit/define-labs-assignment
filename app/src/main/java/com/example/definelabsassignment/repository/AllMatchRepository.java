package com.example.definelabsassignment.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.example.definelabsassignment.model.JsonResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AllMatchRepository {
    private static AllMatchRepository instance;
    private MutableLiveData<JsonResponse> liveData;
    private String TAG = AllMatchRepository.class.getSimpleName();

    public static AllMatchRepository getInstance() {
        if(instance == null)
            instance = new AllMatchRepository();
        return instance;
    }

    public MutableLiveData<JsonResponse> getAllMatchData(){
        liveData = new MutableLiveData<>();
        fetchAllMatchData();
        return liveData;
    }

    private void fetchAllMatchData() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                .callTimeout(50, TimeUnit.SECONDS)
                .connectTimeout(20, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS);

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(NetworkCall.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create());
        builder.client(httpClient.build());
        Retrofit retrofit = builder.build();
        NetworkCall json = retrofit.create(NetworkCall.class);
        Call<JsonResponse> call = json.getAllMatches("40.7484,-73.9857","NPKYZ3WZ1VYMNAZ2FLX1WLECAWSMUVOQZOIDBN53F3LVZBPQ","20180616");
        call.enqueue(new Callback<JsonResponse>() {
            @Override
            public void onResponse(Call<JsonResponse> call, Response<JsonResponse> response) {
                Log.d(TAG,response.body().toString());
                liveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<JsonResponse> call, Throwable t) {
                Log.d(TAG,t.getMessage());
            }
        });
    }

}
