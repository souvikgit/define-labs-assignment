package com.example.definelabsassignment.view;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.definelabsassignment.MainActivity;
import com.example.definelabsassignment.R;
import com.example.definelabsassignment.adapter.SavedMatchesAdapter;
import com.example.definelabsassignment.model.LocalData;
import com.example.definelabsassignment.model.Venues;
import com.example.definelabsassignment.viewmodel.SavedMatchesViewModel;

import java.util.List;

public class SavedMatchesFragment extends Fragment {

    private SavedMatchesViewModel mViewModel;
    private RecyclerView recyclerView;

    public static SavedMatchesFragment newInstance() {
        return new SavedMatchesFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.saved_matches_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(SavedMatchesViewModel.class);
        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mViewModel.getAllSavedData();
        mViewModel.getDataList().observe(getViewLifecycleOwner(), new Observer<List<LocalData>>() {
            @Override
            public void onChanged(List<LocalData> localData) {
                SavedMatchesAdapter adapter =  new SavedMatchesAdapter(localData,getActivity());
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                adapter.setClickListener(new SavedMatchesAdapter.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MainActivity.localDatabase.myDao().deleteItem(localData.get(adapter.getPosition()).getId());
                        for(int i=0;i<AllMatchesFragment.selectedVenuList.size();i++){
                            if(AllMatchesFragment.selectedVenuList.get(i).getId().equals(localData.get(adapter.getPosition()).getId())){
                                AllMatchesFragment.selectedVenuList.remove(i);
                            }
                        }
                    }
                });
            }
        });

    }
}