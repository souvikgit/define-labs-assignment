package com.example.definelabsassignment.view;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.definelabsassignment.MainActivity;
import com.example.definelabsassignment.R;
import com.example.definelabsassignment.adapter.AllMatchesAdapter;
import com.example.definelabsassignment.model.JsonResponse;
import com.example.definelabsassignment.model.LocalData;
import com.example.definelabsassignment.model.Venues;
import com.example.definelabsassignment.viewmodel.AllMatchesViewModel;

import org.json.JSONArray;

import java.util.LinkedList;
import java.util.List;

public class AllMatchesFragment extends Fragment {

    private AllMatchesViewModel mViewModel;
    private String TAG = AllMatchesFragment.class.getSimpleName();
    private RecyclerView recyclerView;
    public static List<Venues> selectedVenuList = null;

    public static AllMatchesFragment newInstance() {
        return new AllMatchesFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.all_matches_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (selectedVenuList == null) {
            selectedVenuList = new LinkedList<>();
            List<LocalData> localData = MainActivity.localDatabase.myDao().getMatchData();
            for (LocalData i : localData) {
                Venues venues = new Venues();
                venues.setId(i.getId());
                venues.setName(i.getName());
                selectedVenuList.add(venues);
            }
        }
        mViewModel = ViewModelProviders.of(this).get(AllMatchesViewModel.class);
        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mViewModel.initiateSearch();
        mViewModel.getMatchDetails().observe(getViewLifecycleOwner(), new Observer<JsonResponse>() {
            @Override
            public void onChanged(JsonResponse jsonArray) {
                Log.d(TAG, jsonArray.toString());
                AllMatchesAdapter adapter = new AllMatchesAdapter(jsonArray.getResponse().getVenues(), selectedVenuList, getActivity());
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                adapter.setClickListener(new AllMatchesAdapter.OnClick() {
                    @Override
                    public void onClick(View v) {
                        selectedVenuList = adapter.getSelectedList();
                    }
                });
            }
        });
    }
}