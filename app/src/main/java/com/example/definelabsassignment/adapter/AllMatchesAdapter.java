package com.example.definelabsassignment.adapter;

import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.definelabsassignment.MainActivity;
import com.example.definelabsassignment.R;
import com.example.definelabsassignment.model.JsonResponse;
import com.example.definelabsassignment.model.LocalData;
import com.example.definelabsassignment.model.Venues;

import java.util.List;

public class AllMatchesAdapter extends RecyclerView.Adapter<AllMatchesAdapter.HolderClass> {
    private List<Venues> list;
    private List<Venues> selectedList;
    private Context context;
    private OnClick clickListener;
    private int position;

    public AllMatchesAdapter(List<Venues> list, List<Venues> selectedList, Context context) {
        this.list = list;
        this.selectedList = selectedList;
        this.context = context;
    }

    @NonNull
    @Override
    public HolderClass onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new HolderClass(LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull HolderClass holder, int position) {
        holder.textView.setText(list.get(position).getName());
        if (checkForIsSelected(list.get(position))) {
            holder.starImage.setImageDrawable(context.getDrawable(R.drawable.ic_filled_star));
        } else {
            holder.starImage.setImageDrawable(context.getDrawable(R.drawable.ic_empty_star));
        }
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPosition(position);
                if (checkForIsSelected(list.get(position))) {
                    selectedList.remove(list.get(position));
                    holder.starImage.setImageDrawable(context.getDrawable(R.drawable.ic_empty_star));
                } else {
                    selectedList.add(list.get(position));
                    if (MainActivity.localDatabase.myDao().isIdPresent(list.get(position).getId()) == 0) {
                        LocalData obj = new LocalData();
                        obj.setId(list.get(position).getId());
                        obj.setName(list.get(position).getName());
                        MainActivity.localDatabase.myDao().insert(obj);
                    }
                    holder.starImage.setImageDrawable(context.getDrawable(R.drawable.ic_filled_star));
                }
                clickListener.onClick(v);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class HolderClass extends RecyclerView.ViewHolder {
        ImageView starImage;
        TextView textView;
        ConstraintLayout container;

        public HolderClass(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.text);
            container = itemView.findViewById(R.id.container);
            starImage = itemView.findViewById(R.id.start_img);
        }
    }

    private boolean checkForIsSelected(Venues venues) {
        for (Venues i : selectedList) {
            if (venues.getId().equals(i.getId())) {
                return true;
            }
        }
        return false;
    }

    public void setClickListener(OnClick clickListener) {
        this.clickListener = clickListener;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public List<Venues> getSelectedList() {
        return selectedList;
    }

    public void setSelectedList(List<Venues> selectedList) {
        this.selectedList = selectedList;
    }

    public interface OnClick extends View.OnClickListener {
        @Override
        void onClick(View v);
    }
}
