package com.example.definelabsassignment.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.definelabsassignment.R;
import com.example.definelabsassignment.model.LocalData;
import com.example.definelabsassignment.repository.LocalDao;

import java.util.List;

public class SavedMatchesAdapter extends RecyclerView.Adapter<SavedMatchesAdapter.HolderClass> {

    private List<LocalData> list;
    private Context context;
    private OnClickListener clickListener;
    private int position;

    public SavedMatchesAdapter(List<LocalData> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public SavedMatchesAdapter.HolderClass onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new HolderClass(LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull SavedMatchesAdapter.HolderClass holder, int position) {
        holder.textView.setText(list.get(position).getName());
        holder.starImage.setImageDrawable(context.getDrawable(R.drawable.ic_filled_star));
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.starImage.setImageDrawable(context.getDrawable(R.drawable.ic_empty_star));
                setPosition(position);
                clickListener.onClick(v);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class HolderClass extends RecyclerView.ViewHolder {
        ImageView starImage;
        TextView textView;
        ConstraintLayout container;
        public HolderClass(@NonNull View itemView) {
            super(itemView);
            starImage = itemView.findViewById(R.id.start_img);
            textView = itemView.findViewById(R.id.text);
            container = itemView.findViewById(R.id.container);
        }
    }

    public OnClickListener getClickListener() {
        return clickListener;
    }

    public void setClickListener(OnClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public interface OnClickListener extends View.OnClickListener{
        @Override
        void onClick(View v);
    }
}
